#include "processor.h"

Processor::Processor (Memory * m):m (m)
{
  pc = 1024;			       // start the program at address 0
  for (int i = 0; i < 16; i++)
    r[i] = 0;
  r[1] = 1;			       // and it should never change again
  flag = 0;
}

Processor::~Processor ()
{
}



void
Processor::von_Neuman_step (bool debug)
{
  // various variables for convenience
  uint32_t old_pc;


  if (debug)
    {				       // show state before the instruction
      std::cout << "Before instruction: pc=" << std::hex << std::
	setw (8) << std::setfill ('0') << pc << " pointing to ";
      for (int i = 0; i < 16; i++)
	std::cout << std::hex << std::setw (1) << m->read (pc + i);
      std::cout << "..." << std::endl;
    }

  old_pc = pc;		       // mostly for debug


  // Fetch first nibble of the instruction
  uint32_t instr = m->read (pc);
  pc = pc + 1;		       // yes I know I could write  m->read(pc++)  but I won't.

  // Execute
  switch (instr)
    {
    case 0x0: // j (reative jump)
      {
	// read two more nibbles
	int32_t delta;
	delta = (m->read (pc)) << 24;	// put it on top of a 32-bit word
	pc++;
	delta += (m->read (pc)) << 28;	//
	pc++;
	delta = delta >> 24;   // shift it back to the lower byte: this performs the sign extension
	pc = old_pc + delta;
	if (pc == old_pc)
	  std::cout << "Fin normale du programme" << std::endl;
	break;
      }
    case 0x1: // jt (conditional relative jump)
      {
	int32_t delta;
	delta = (m->read (pc)) << 24;	// put it on top of a 32-bit word
	pc++;
	delta += (m->read (pc)) << 28;	//
	pc++;
	delta = delta >> 24;   // shift it back to the lower byte: this performs the sign extension
	if (flag)
	  pc = old_pc + delta;
	break;
      }
    case 0x2: // jf (conditional relative jump)
      {
	int32_t delta;
	delta = (m->read (pc)) << 24;	// put it on top of a 32-bit word
	pc++;
	delta += (m->read (pc)) << 28;	//
	pc++;
	delta = delta >> 24;   // shift it back to the lower byte: this performs the sign extension
	if (!flag)
	  pc = old_pc + delta;
	break;
      }
    case 0x3: // add
      {
	uint32_t x, y, realry, result;
	x = m->read (pc++);
	y = m->read (pc++);
	if (y == 0 && flag)
	  realry = 1;
	else
	  realry = r[y];
	if (x != 0 && x != 1)
	  {
	    result = r[x] + realry;
	    // We check if there was a carry or not
	    if ((r[x] & (0x1 << 31) && r[y] & (0x1 << 31))
		|| ((r[x] & (0x1 << 31) || r[y] & (0x1 << 31)) && (~result & (0x1 << 31))))
	      flag = 1;
	    else
	      flag = 0;
	    // We store the result in the register
	    r[x] = result;
	  }
	break;
      }
    case 0x4: // sub
      {
	uint32_t x, y, result;
	x = m->read (pc++);
	y = m->read (pc++);
	if (x != 0 && x != 1)
	  {
	    result = r[x] - r[y];
	    // We check if there was a carry or not
	    if ((r[x] & (0x1 << 31) && r[y] & (0x1 << 31))
		|| ((r[x] & (0x1 << 31) || r[y] & (0x1 << 31)) && (~result & (0x1 << 31))))
	      flag = 1;
	    else
	      flag = 0;
	    // We store the result in the register
	    r[x] = result;
	  }
	break;
      }
    case  0x5: // copy
      {
	uint32_t x, y;
	x = m->read(pc++);
	y = m->read(pc++);
	if (x == 0)
	  pc = r[y];
	else if (x == 1)
	  r[y] = pc;
	else
	  r[x] = r[y];
	break;
      }
    case 0x6: // shift
      {
	uint32_t x, y, shift=0;
	bool sign;
	x = m->read(pc++);
	y = m->read(pc++);
	sign = (x & 0x8) != 0;
	x = x & (~0x8); // removing the direction of the shift
	switch (x)
	  {
	  case 0x0: { shift = 1; break; }
	  case 0x1: { shift = 2; break; }
	  case 0x2: { shift = 3; break; }
	  case 0x3: { shift = 4; break; }
	  case 0x4: { shift = 8; break; }
	  case 0x5: { shift = 12; break; }
	  case 0x6: { shift = 16; break; }
	  case 0x7: { shift = 24; break; }
	  }
	if (y != 0 && y != 1)
	  {
	    if (!sign)
	      {
		if (shift == 1)
		  flag = r[y] & (0x1 << 31);
		r[y] = r[y] << shift;
	      }
	    else
	      {
		if (shift == 1)
		  flag = r[y] & 0x1;
		r[y] = r[y] >> shift;
	      }
	  }
	break;
      }
    case 0x8: // isequal
      {
	uint32_t x, y;
	x = m->read(pc++);
	y = m->read(pc++);
	flag = r[x]==r[y];
	break;
      }
    case 0x9: // isgt
      {
	uint32_t x, y;
	x = m->read(pc++);
	y = m->read(pc++);
	if ((r[x] & (0x1 << 31)) && (r[y] & (0x1 << 31))) // If Rx and Ry are negative
	  flag = r[y] > r[x];
	else if (r[x] & (0x1 << 31)) // If only Rx is negative
	  flag = 0;
	else if (r[y] & (0x1 << 31)) // If only Ry is negative
	  flag = 1;
	else // If Rx and Ry are positive
	  flag = r[x] > r[y];
	break;
      }
    case 0xA: // mr (memory read) & mru (memory read update)
      {
	uint32_t X, x, y, n, i, addr;
	int32_t result = 0;
	// opcode has form of AX
	X = m->read(pc++);
	if (X < 8)
		n = X + 1;     // memory read case
	else
		n = X - 7;     // memory read update case
	x = m->read(pc++);
	y = m->read(pc++);
	if (y == 0)
		addr = old_pc;
	else
		addr = r[y];
	for (i=0; i < n; i++)
		result += (m->read(addr+i)) << (32 - 4*(n-i));
	result = result >> (32 - 4*n);
	if (x != 0 && x != 1)
		r[x] = result;
	if (x == 0)
		pc = result;
	// in case of mru update r[y]
	if (X >= 8 && y != 0 && y != 1)
		r[y] += n;
	break;
      }
	case 0xB: // mw (memory write) & mwu (memory write update)
	  {
	uint32_t X, x, y, n, i, content;
	// opcode has form of BX
	X = m->read(pc++);
	if (X < 8)
		n = X + 1;     // memory write case
	else
		n = X - 7;     // memory write update case
	x = m->read(pc++);
	y = m->read(pc++);
	if (x == 0)
		content = pcra;
	else
		content = r[x];
	for (i=0; i < n; i++)
	{
		m->write(r[y]+i, content & 0xF);
		content >>= 4;
	}
	if (X >= 8 && y != 0 && y != 1)
		// in case of mwu update r[y]
		r[y] += n;
	break;
	  }
	case 0xC: // syscall & push
	  {
	    uint32_t X, x, y, n, i, content;
	    X = m->read(pc++);
	    if (X < 8) // in case of a syscall
	      {
		uint32_t new_pc = 0;
		n = ((X << 4) + m->read(pc++)) * 8;
		for (i=0; i<8; i++)
		  new_pc += (m->read(i+n)) << 4*i;
		pc = new_pc;
		pcra = old_pc + 3;
	      }
	    else {  // in case of a push
	      n = X - 7;
	      x = m->read(pc++);
	      y = m->read(pc++);
	      if (y == 0)
		content = pcra;
	      else
		content = r[y];
	      for (i=0; i < n; i++)
		m->write(r[x]-i-1, (content >> (4*(n-i-1))) & 0xF);
	      r[x] -= n;
	    }
	    break;
	  }
	case 0xD: // call
		{
	uint32_t addr, i;
	addr = 0;
	pcra = old_pc + 9;
	for (i = 0;i < 8; i++)
		addr += (m->read(pc++)) << 4*i;
	pc = addr;
	break;
		}
	case 0xE:
		{
	pc = pcra;
	break;
		}
    case 0xF: // logical operations on bits or constants
      {
	uint32_t op;
	op = m->read(pc++);
	switch (op)
	  {
	  case 0x0: // not
	    {
	      uint32_t x, y;
	      x = m->read(pc++);
	      y = m->read(pc++);
	      if (x != 0 && x != 1)
		r[x] = ~r[y];
	      break;
	    }
	  case 0x1: // and
	    {
	      uint32_t x, y;
	      x = m->read(pc++);
	      y = m->read(pc++);
	      if (x != 0 && x != 1)
		r[x] = r[x] & r[y];
	      break;
	    }
	  case 0x2: // or
	    {
	      uint32_t x, y;
	      x = m->read(pc++);
	      y = m->read(pc++);
	      if (x != 0 && x != 1)
		r[x] = r[x] | r[y];
	      break;
	    }
	  case 0x3: // xor
	    {
	      uint32_t x, y;
	      x = m->read(pc++);
	      y = m->read(pc++);
	      if (x != 0 && x != 1)
		r[x] = r[x] ^ r[y];
	      break;
	    }
	  case 0x4: // ld2
	    {
	      uint32_t x, i, result = 0;
	      x = m->read(pc++);
	      for (i=0; i < 2; i++)
		result += (m->read(pc++)) << (32 - 4*(2-i));
	      result = result >> 24;
	      if (x != 0 && x != 1)
		r[x] = result;
	      break;
	    }
	  case 0x5: // ld4
	    {
	      uint32_t x, i, result = 0;
	      x = m->read(pc++);
	      for (i=0; i < 4; i++)
		result += (m->read(pc++)) << (32 - 4*(4-i));
	      result = result >> 16;
	      if (x != 0 && x != 1)
		r[x] = result;
	      break;
	    }
	  case 0x6: // ld8
	    {
	      uint32_t x, i, result = 0;
	      x = m->read(pc++);
	      for (i=0; i < 8; i++)
		result += (m->read(pc++)) << (32 - 4*(8-i));
	      if (x != 0 && x != 1)
		r[x] = result;
	      break;
	    }
	  }
	break;
      }
    default:
      std::cerr << "Illegal instruction: " << std::
	hex << instr << " at PC=" << pc << std::endl;
      // and that's it, we proceed with execution
      break;
    }


  if (debug)
    {
      std::cout << "After instruction,  pc=" << std::hex << std::
	setw (8) << std::setfill ('0') << pc;
	  std::cout << ", pcra=" << std::hex << std::
	setw (8) << std::setfill ('0') << pcra;
      std::cout << std::endl ;
	  std::cout << "Flag state: " << flag;
      for (int i = 0; i < 16; i++)
	std::cout << ((i & 3) ==
		      0 ? "\n" : "") << " r" << std::
	  hex << i << " = " << std::hex << std::setw (8) << std::
	  setfill ('0') << r[i] << " ";
      std::cout << std::endl;
    }
}
