#ifndef H_SCREEN
#define H_SCREEN
#include"memory.h"
//constants about the environnement
static const int WIDTH = 320;
static const int HEIGHT = 256;
// screen at the end of the memory; each pixel is a 12-bit word hence the *3
static const int MEM_SCREEN_BEGIN = MEM_SIZE-HEIGHT*WIDTH*3;

#include <SDL2/SDL.h>
#include "memory.h"

/* this is the function that runs in the screen thread
 * m -> the simulator's memory
 * force_quit -> shared variable to detect if we must close the screen
 * refresh -> shared variable that instructs the thread to refresh the screen
 */
void simulate_screen(Memory* m, bool *refresh);

#endif
