#include<inttypes.h>	// defines among other uint16_t (signed) and int16_t (signed) for various values of 16.

#include "memory.h"

class Processor
{
public:
  Processor (Memory * m);
  ~Processor ();
  void von_Neuman_step (bool debug);

private:
  uint32_t pc;
  uint32_t pcra;
  uint32_t flag;		// what a waste, 32 bits for 1 bit
  // The general purpose registers
  uint32_t r[16];

  Memory *m;
};
