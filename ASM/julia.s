#include bootloader/boot.s

	;; Le nombre de termes de la suite qu'on calcule pour chaque
	;; point
#const LIMITE 100
	;; La graine de l'ensemble de Julia qu'on veut calculer
#const PARTIE_RELLE 0x00490000		; ~0.285
#const PARTIE_IMAGINAIRE 0x00035800	; ~0.013
;;; On implémente ici un dessin de la fractale de Julia
;;; On la dessine pour Im(z) \in [-1.5;1.5] et \Re(z) \in [-2;1]
;;; On utilise une représentation des nombres complexes par une paire
;;; de nomres à virgule fixe de la forme a + ib


	call main

	;; La fonction appelée sur chaque point
	;; Elle vérifie si la suite associée au point converge ou pas
	;; Puis elle l'affiche aux bonnes coordonnées
	;; 1. c = a
	;; 2. d = b
	;; 3. i = 0
	;; 4. tant que c²+d² < 4 et i < LIMITE
	;; 	5. c = c²-d² + @PARTIE_REELLE
	;; 	6. d = 2*c*d + @PARTIE_IMAGINAIRE
	;; 	7. i++
	;; 7. draw (a,b,i)
mandel_plot:
	;; PROLOGUE
	push8 rf 0		; save pcra
	push8 rf r2
	push8 rf r3
	push8 rf r4
	push8 rf r5
	push8 rf r6
	push8 rf r7
	push8 rf r8
	push8 rf r9
	push8 rf ra
	push8 rf rb
	push8 rf rc
	push8 rf rd
	
	copy r5 r3		; r5 = a
	copy r6 r2		; r6 = b
	copy r9 r3		; r9 = c
	copy ra r2		; ra = d
	copy r7 0		; r7 = i
db_mandel_1:
	ld4 r8 @LIMITE		; r8 = LIMITE
	isequal r7 r8
	jt fb_mandel_1
	copy r2 r9
	copy r3 r9
	syscall 8
	copy rb r4		; r4 = c²
	copy rc rb
	copy r2 ra
	copy r3 ra
	syscall 8
	copy rd r4
	add r4 rb		; r4 = c²+d²
	ld8 rb 0x08000000	; rb = 4
	isgt r4 rb
	jt fb_mandel_1
	;; corps de la boucle
	copy rb rc		; r4 = c²
	sub rb rd
	copy rc r9		; on sauvegarde c pour calculer d
	copy r9 rb		; c = c²-d²
	ld8 r8 @PARTIE_RELLE
	add r9 r8		; c += a
	copy r2 rc
	copy r3 ra
	syscall 8
	shiftl1 r4
	ld8 r8 @PARTIE_IMAGINAIRE
	add r4 r8
	copy ra r4		; d = 2cd + b
	add r7 1		; i++
	j db_mandel_1
fb_mandel_1:
	copy r2 r5
	copy r3 r6
	copy r4 r7
	call draw_point_mandel
	
	;; EPILOGUE
	mru8 rd rf
	mru8 rc rf
	mru8 rb rf
	mru8 ra rf
	mru8 r9 rf
	mru8 r8 rf
	mru8 r7 rf
	mru8 r6 rf
	mru8 r5 rf
	mru8 r4 rf
	mru8 r3 rf
	mru8 r2 rf
	mru8 r0 rf ; alternative ret



	;; draw_point_mandel
	;; prend en paramètres les coordonnées r3,r2 et le compteur r4
	;; affiche le pixel somehow
	;; 1. r2 = r2 + 2
	;; 2. r3 = r3 + 1.5
	;; 3. r2 = r2 >> 14
	;; 4. r3 = r3 >> 14
	;; 5. r4 = f(r4) pour un certain f plutôt stylé
	;; 5. syscall plot
draw_point_mandel:
	;; PROLOGUE
	push8 rf r0
	push8 rf r2
	push8 rf r3
	push8 rf r4
	push8 rf r5
	push8 rf r6
	;; CORPS
	ld8 r5 0x03000000
	add r2 r5
	ld8 r5 0x027F0000
	add r3 r5
	shiftr16 r2
	shiftr2 r2
	shiftr16 r3
	shiftr2 r3
	ld8 r5 @LIMITE
	isequal r4 r5
	jt couleur_yup
	isequal r4 0
	jt couleur_fin_sub
	sub r4 1
	isequal r4 0
	jt couleur_fin_sub
	sub r4 1
couleur_fin_sub:
	ld2 r5 0xF
	isgt r4 r5
	jf couleur_nup
	copy r4 r5
couleur_nup:
	copy r5 r4
	copy r6 r5
	shiftr1 r6
	copy r5 r6
	shiftr2 r6
	add r5 r6
	shiftl4 r5
	add r4 r5
	shiftl4 r5
	;add r4 r5
	j fin_couleurs
couleur_yup:
	ld4 r4 0x000
fin_couleurs:
	syscall 2
	;; EPILOGUE
	mru8 r6 rf
	mru8 r5 rf
	mru8 r4 rf
	mru8 r3 rf
	mru8 r2 rf
	mru8 r0 rf

	
	
	;; La fonction principale
	;; z = a + ib
	;; 1. pour a de 2.5 à -1.5
	;; 2. pour b de -2 à 3
	;; 3. plot_julia (b,a)
	;;
	;; (3/255) = 0x3030
main:
	ld8 r2 0x01800000	; r2 = 1.5
	ld8 r3 0xFD000000	; r3 = -2
	ld8 r4 0x0000	; r4 = -2.5
	ld8 r5 0x0000	; r5 = 1
	ld8 r6 0x00040000		; r6 = 4/256
	copy r7 r3		; r7 = -2
	ld8 r8 255
	ld8 r9 320

debut_boucle_1:
	isgt r4 r8
	jt fin_boucle_1
	copy r3 r7
debut_boucle_2:
	isgt r5 r9
	jt fin_boucle_2
	call mandel_plot
	add r3 r6
	add r5 1
	j debut_boucle_2
fin_boucle_2:
	sub r2 r6
	ld8 r5 0x0000
	add r4 1
	j debut_boucle_1
fin_boucle_1:
	;; fin du programme
	
