#!/usr/bin/env python3
# -*- coding: UTf-8 -*-
import os
import sys

def analyze(path):
    basename = path.strip().split('/+')[-1]
    idx = basename.find('.')
    return (basename[:idx],basename[idx+1:])

def matching(text1, text2):
    matching_chars = 0
    if (len(text1) != len(text2)):
        return (-1)
    else:
        size = len(text1)
        for idx in range( size ):
            if (text1[idx] == text2[idx]):
                matching_chars += 1
        return (size, size - matching_chars, matching_chars*1.0 / size*1.0)

if __name__ == "__main__":
    files = []
    if len(sys.argv) == 3:
        if (sys.argv[1] == '-t'):
            files = [ analyze(sys.argv[2]) ]
    else:
        files = [analyze(file) for file in os.listdir('.')]
    sources = [basename for basename, ext in files if ext == 's']
    sources.sort()
    print("-----------------------------------------------")
    print("####    Starting Test Session              ####")
    print("-----------------------------------------------")

    for source_file in sources:


        CMD_OLD = '../asm.py %s.s' % source_file
        CMD_NEW = '../asm.src/asm.py %s.s -v -o %s.new.obj -s' % (source_file, source_file)

        print(" -*- Start test: %s.s" % source_file)
        print(" -*- Commands:  ", CMD_OLD, "   ", CMD_NEW)

        os.system(CMD_OLD)
        os.system(CMD_NEW)

        OBJ_OLD = open('%s.obj' % source_file, 'r')
        OBJ_NEW = open('%s.new.obj' % source_file, 'r')

        print(" -*- Test result:   ", matching(OBJ_OLD.read(), OBJ_NEW.read()))

        input(" -*- End of current test. Would you like to proceed to next one ? ")

    print("-----------------------------------------------")
    print("####    Ending Test Session                ####")
    print("-----------------------------------------------")