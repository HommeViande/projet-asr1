	;; prend en argument les coordonnées (r2,r3), (r4,r5) et une couleur r6
;;; 1. On normalise les coordonnées (r3 <- 255 - r3, r5 <- 255 - r5, et on réarrange
;;;    de sorte que r2 < r4 et r3 < r5
;;; 2. Pour chaque ligne
;;; 3. Pour chaque colonne
;;; 4. On met la couleur
;;; r7 : calculs inintéressants
;;; r8 : fin de la ligne
;;; r9 : ancien r3
;;; ra : taille d'une ligne
    ;; fill:
	push8 rf r2
	push8 rf r3
	push8 rf r4
	push8 rf r5
	push8 rf r7
	push8 rf r8
	push8 rf r9
	push8 rf ra

	ld8 r7 255
	sub r7 r3
	copy r3 r7		; r3 <- 255 - r3
	ld8 r7 255
	sub r7 r5
	copy r5 r7		; r5 <- 255 - r5
	isgt r2 r4
	jf test1_true_fill
	copy r7 r4
	copy r4 r2
	copy r2 r7		; r2 < r4
test1_true_fill:
	isgt r3 r5
	jf test2_true_fill
	copy r7 r5
	copy r5 r3
	copy r3 r7		; r3 < r5
test2_true_fill:
	copy r7 r3
	shiftl8 r7
	shiftl4 r3
	shiftl2 r3
	add r3 r7		; r3 = 320 * r3
	add r3 r2		; r3 = r3 + r2
	copy r7 r3
	add r3 r3
	add r3 r7		; r3 = 3*r3 (r3 est la première adresse à laquelle on veut écrire)
	ld8 ra 0xC4000
	add r3 ra
	copy r7 r5
	shiftl8 r7
	shiftl4 r5
	shiftl2 r5
	add r5 r7		; r5 = 320 * r5
	add r5 r4		; r5 = r5 + r4
	copy r7 r5
	add r5 r5
	add r5 r7		; r5 = 3*r5 (r5 est la dernière adresse à laquelle on veut écrire)
	add r5 ra
	sub r4 r2		; r4 = longueur d'une ligne du rectangle
	add r4 1
	ld8 ra 960		; ra = taille en mémoire d'une ligne de l'écran
debut_boucle1_fill:
	isgt r3 r5
	jt fin_boucle1_fill
	copy r8 r4
	copy r9 r3
debut_boucle2_fill:
	isequal r8 0
	jt fin_boucle2_fill
	mwu3 r6 r3
	sub r8 1
	j debut_boucle2_fill
fin_boucle2_fill:
	add r9 ra
	copy r3 r9
	j debut_boucle1_fill
fin_boucle1_fill:

	mru8 ra rf
	mru8 r9 rf
	mru8 r8 rf
	mru8 r7 rf
	mru8 r5 rf
	mru8 r4 rf
	mru8 r3 rf
	mru8 r2 rf
	ret