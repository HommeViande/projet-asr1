;;; code pseudo-C :
;;; entrée : deux entiers A et B
;;; 1. C = 0
;;; 2. while A != 0
;;; 3.   if (A&1 == 1) then C=C+B
;;; 4.   B = B << 1;
;;; 5.   A = A >> 1;
;;; 6. return C;

;;; A : r2
;;; B : r3
;;; C : r4

;; PROLOGUE
	push8 rf 0  ; save pcra
	push8 rf r2 
	push8 rf r3
	push8 rf r5

;; CORPS
	copy r4 r0		; 1.
	isequal r0 r2
	jt 28 			; 2.
	copy r5 r2
	and r5 r1
	isequal r5 r1
	jf 6
	add r4 r3		; 3.
	shiftl1 r3		; 4.
	shiftr1 r2		; 5.
	j -28

;; EPILOGUE
	mru8 r5 rf
  	mru8 r3 rf
  	mru8 r2 rf 
  	mru8 r0 rf ; alternative ret

;;; La première ligne est toujours exécutée 1 fois
;;; La boucle principale est exécutée au plus 32 fois
;;; Le saut conditionnel à l'intérieur de la boucle est effectué au
;;; moins 0 fois
;;; Au total, on a donc, au pire cas, 3 + 31*32 + 6 = 1001 mioches de lus.
