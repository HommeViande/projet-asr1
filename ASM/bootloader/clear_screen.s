	;; prend en argument r2
	;; remplit l'écran avec la couleur représentée dans r2
    ;; clear_screen:
	push8 rf r3
	push8 rf r4
	ld8 r3 0xC4000		; adresse du début de l'écran
	ld8 r4 0x100000		; dernière adresse de la mémoire
debut_boucle_clear_screen:
	isequal r3 r4
	jt end_clear_screen
	mwu3 r2 r3
	j debut_boucle_clear_screen
end_clear_screen:
	mru8 r4 rf
	mru8 r3 rf
	ret