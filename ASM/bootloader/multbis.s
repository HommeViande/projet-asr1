;;; code pseudo-C :
;;; entrée : deux entiers A et B
;;; sortie : A*B
;;; 0. C=0
;;; 1. negs = 0
;;; 2. if A&80000000 == 80000000 then
;;; 3.   negs = 1
;;; 4.   A = ~A + 1
;;; 5. if B&80000000 == 80000000 then
;;; 6.   negs = negs ^ 1
;;; 7.   B = ~B + 1
;;; 8. while A != 0
;;; 9.   if (A&1 == 1) then C=C+B
;;; 10   B = B << 1
;;; 11   A = A >> 1
;;; 12 if negs == 1 then C = ~C + 1

;;; A = r2
;;; B = r3
;;; C = r4
;;; negs = r5

;; PROLOGUE
	push8 rf 0  ; save pcra
	push8 rf r2 
	push8 rf r3
	push8 rf r5
	push8 rf r6
	push8 rf r7

;; CORPS
	copy r4 0		; 0.
	copy r5 0		; 1.
	ld8 r6 0x80000000
	copy r7 r6
	and r6 r2
	isequal r7 r6		; 2.
	jf 13
	copy r5 1		; 3.
	not r2 r2
	add r2 1		; 4.
	copy r6 r7
	and r6 r3
	isequal r7 r6		; 5.
	jf 14
	xor r5 1		; 6.
	not r3 r3
	add r3 1		; 7.
	isequal 0 r2		; 8.
	jt 28
	copy r6 r2
	and r6 r1
	isequal r6 r1		; 9.
	jf 6
	add r4 r3		; 9.
	shiftl1 r3		; 10.
	shiftr1 r2		; 11.
	j -28
	isequal 0 r5
	jt 10
	not r4 r4
	add r4 1		; 12.

;; EPILOGUE
	mru8 r7 rf
	mru8 r6 rf
	mru8 r5 rf
  	mru8 r3 rf
  	mru8 r2 rf 
  	mru8 r0 rf ; alternative ret

;;; Au cour de l'exécution de ce programme, le processeur va lire au
;;; plus 64 + 31*32 + 9 = 1065 mioches (dans le cas où les deux
;;; paramètres sont négatifs)
