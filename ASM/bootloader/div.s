;; code pseudo-C :
;; entrées : deux entiers X et D
;; sortie  : deux entiers Q et R
;; Q = 0; R = 0
;; 1.  Q <- 0
;; 2.  R <- 0
;; 3.  while X != 0
;; 4.    R <- (R << 1)
;; 5.    if (X & (1 << 31))
;; 6.      R <- R + 1
;; 7.    X <- (X << 1)
;; 8.    Q <- (Q << 1)
;; 9.    if (R >= D)
;; 10.      R <- (R - D)
;; 11.      Q <- Q + 1
;; 12. return (Q, R);
;;
;; Dans le code assembleur
;; X = r2
;; D = r3
;; Q = r4
;; R = r5

    isequal r3 r0     ;; Teste si on ne divise pas par zéro
    jt fin
    copy r4 r0
    copy r5 r0
boucle:
    isequal r2 r0
    jt fin
    shiftl1 r5
    shiftl1 r2
    add r5 0
    shiftl1 r4
    isgt r5 r3
    jf 9
    sub r5 r3
    add r4 r1
    isequal r5 r3
    jf boucle
    sub r5 r3
    add r4 r1
    j boucle
fin:


;;; Les quartre premières lignes sont toujours exécutées 1 fois
;;; La boucle principale est exécutée au plus 32 fois
;;; Le corps des structure de contrôle à l'intérieur de la boucle sont exécutés au plus une fois chacun
;;; Au total, on a donc, au pire cas, 12 + (14 + 18)*32 = 1036 mioches de lus.
