#!/usr/bin/env python3
# -**- coding: UTF-8

# This program assembles source assembly code into a hex string.
# The hex string may include spaces and newlines for readability,
# these should be ignored by the simulator when it reads the corresponding file.

import os
import sys
import re
import string
import argparse

from assembler import Assembler

# /* main */
if __name__ == '__main__':

    argparser = argparse.ArgumentParser(description='This is the assembler for the ASR2019 processor @ ENS-Lyon')
    argparser.add_argument('filename', help='name of the source file.  "python asm.py toto.s" assembles toto.s into toto.obj')
    argparser.add_argument("-v", "--verbose", action="store_true",
                    help="increase output verbosity")
    argparser.add_argument("-s", "--saveLineFormat", action="store_true",
                    help="conserves line return in the compiled object file")
    argparser.add_argument("-o", "--output", type=str,
                    help="optional output filename")
    

    options = argparser.parse_args()
    filename = options.filename
    basefilename, extension = os.path.splitext(filename)
    obj_file = basefilename + ".obj"
    if  (options.output):
        obj_file = options.output

    print("Compiling %s:" % filename)

    assemembler = Assembler(filename, options.saveLineFormat, options.verbose)
    assemembler.assemble()

    print("Outputing compiled file in " + obj_file)
    print("Average instruction size is " + str(assemembler.avgInstructionSize) + " nibbles")
    outfile = open(obj_file, "w")
    outfile.write(assemembler.compiledCode)
    outfile.close()
