"""
    Logger class
"""

class Logger(object):

    def __init__(self, debugEnable, debugHeader, prefixSpaces = 0, muteInfo = False):
        self.debugEnable = debugEnable
        self.debugHeader = debugHeader
        self.prefixSpaces = prefixSpaces
        self.muteInfo = muteInfo
    
    def debug(self, *args):
        if (self.debugEnable):
            print(self.debugHeader, " ", *args)

    def info(self, *args):
        if (not self.muteInfo):
            print(" " * self.prefixSpaces, *args)
